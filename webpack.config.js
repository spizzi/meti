const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = (env) => {
  return {
    entry: "./src/App.tsx",
    mode: "development",
    devtool: 'eval-source-map',
    output: {
      path: path.join(__dirname, "/dist"), // the bundle output path
      filename: "bundle.js", // the name of the bundle
      publicPath: '/'
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          { from: "./public", to: "." }, // Copies the content of "from" folder into output folder.
        ],
      }),
    ],
    devServer: {
      host: '0.0.0.0',
      port: 3000, // you can change the port
      historyApiFallback: true,
      allowedHosts: "all",
      static: ["./public"],
      liveReload: true
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          loader: "ts-loader",
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
              options: {
                modules: {
                  localIdentName: '[local]--[hash:base64:5]'
                }
              }
            }
          ]
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: "babel-loader",
        },
      ],
    },
    resolve: {
      extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
    },
  }
};