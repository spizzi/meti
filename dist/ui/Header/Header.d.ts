interface IHeader {
    title?: string;
    variant?: string;
}
declare const Header: (props: IHeader) => import("react/jsx-runtime").JSX.Element;
export default Header;
