/// <reference types="react" />
interface IMainPage {
    children?: React.ReactNode;
    theme?: "dark" | "light";
}
declare const MainPage: (props: IMainPage) => import("react/jsx-runtime").JSX.Element;
export default MainPage;
