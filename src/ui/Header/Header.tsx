import styles from './header.module.css'

interface IHeader {
    title ?: string,
    variant ?: string,
}

const Header = (props: IHeader) => {

    return (
        <div className={`${styles.headerLayout} ${props.variant}`}>
            <div className={styles.logoWrapper}>
                {/* <Logo /> */}
                {props.title}
            </div>
        </div>
    )
}

export default Header;