import Header from '../Header/Header';
import styles from './mainPage.module.css'

interface IMainPage {
    children ?: React.ReactNode
    theme ?: "dark" | "light"
}

const MainPage = (props: IMainPage) => {

    const mainPageStyle = props.theme === "light" ? `${styles.pageLayout} ${styles.dark_theme}` : styles.pageLayout;

    return (
        <div className={mainPageStyle}>
            <div className={styles.headerWrapper}>
                <Header title='Meti' />
            </div>
            <div className={styles.pageWrapper}>
                <div className={styles.pageContentWrapper}>
                    {props.children}
                    {/* Footer bar */}
                </div>
            </div>
        </div>
    )
}

export default MainPage;