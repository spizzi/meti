import React from 'react';
import ReactDOM from 'react-dom/client';
import MainPage from './ui/MainPage/MainPage';

const AppRoot = () => {
    return (
    	<MainPage theme='dark' />
    )
}

class AppManager extends React.Component {
    componentDidMount () {
    }

    render() {
        return (
			<AppRoot />
        )
    }
}

const rootHtml = document.getElementById("root");
const root = ReactDOM.createRoot(rootHtml);
root.render( <AppManager />);